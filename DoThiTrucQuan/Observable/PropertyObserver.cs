﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Windows;
using System.Linq.Expressions;
using System.Reflection;

namespace DoThiTrucQuan.Observable
{
    public class PropertyObserver<TPropertySource> : IWeakEventListener where TPropertySource : INotifyPropertyChanged
    {
        // Fields
        private readonly Dictionary<string, Action<TPropertySource>> _propertyNameToHandlerMap;
        private readonly WeakReference _propertySourceRef;

        // Methods
        public PropertyObserver(TPropertySource propertySource)
        {
            if (propertySource == null)
            {
                throw new ArgumentNullException("propertySource");
            }
            this._propertySourceRef = new WeakReference(propertySource);
            this._propertyNameToHandlerMap = new Dictionary<string, Action<TPropertySource>>();
        }

        private static string GetPropertyName(Expression<Func<TPropertySource, object>> expression)
        {
            MemberExpression operand;
            LambdaExpression expression2 = expression;
            if (expression2.Body is UnaryExpression)
            {
                UnaryExpression body = expression2.Body as UnaryExpression;
                operand = body.Operand as MemberExpression;
            }
            else
            {
                operand = expression2.Body as MemberExpression;
            }
            if (operand != null)
            {
                PropertyInfo member = operand.Member as PropertyInfo;
                return member.Name;
            }
            return null;
        }

        private TPropertySource GetPropertySource()
        {
            try
            {
                return (TPropertySource)this._propertySourceRef.Target;
            }
            catch
            {
                return default(TPropertySource);
            }
        }

        public PropertyObserver<TPropertySource> RegisterHandler(Expression<Func<TPropertySource, object>> expression, Action<TPropertySource> handler)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            string propertyName = PropertyObserver<TPropertySource>.GetPropertyName(expression);
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("'expression' did not provide a property name.");
            }
            if (handler == null)
            {
                throw new ArgumentNullException("handler");
            }
            TPropertySource propertySource = this.GetPropertySource();
            if (propertySource != null)
            {
                this._propertyNameToHandlerMap[propertyName] = handler;
                PropertyChangedEventManager.AddListener(propertySource, this, propertyName);
            }
            return (PropertyObserver<TPropertySource>)this;
        }

        bool IWeakEventListener.ReceiveWeakEvent(Type managerType, object sender, EventArgs e)
        {
            bool flag = false;
            if (managerType == typeof(PropertyChangedEventManager))
            {
                Action<TPropertySource> action2;
                PropertyChangedEventArgs args = e as PropertyChangedEventArgs;
                if ((args == null) || !(sender is TPropertySource))
                {
                    return flag;
                }
                string propertyName = args.PropertyName;
                TPropertySource local = (TPropertySource)sender;
                if (string.IsNullOrEmpty(propertyName))
                {
                    foreach (Action<TPropertySource> action in this._propertyNameToHandlerMap.Values.ToArray<Action<TPropertySource>>())
                    {
                        action(local);
                    }
                    return true;
                }
                if (this._propertyNameToHandlerMap.TryGetValue(propertyName, out action2))
                {
                    action2(local);
                    flag = true;
                }
            }
            return flag;
        }

        public PropertyObserver<TPropertySource> UnregisterHandler(Expression<Func<TPropertySource, object>> expression)
        {
            if (expression == null)
            {
                throw new ArgumentNullException("expression");
            }
            string propertyName = PropertyObserver<TPropertySource>.GetPropertyName(expression);
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException("'expression' did not provide a property name.");
            }
            TPropertySource propertySource = this.GetPropertySource();
            if ((propertySource != null) && this._propertyNameToHandlerMap.ContainsKey(propertyName))
            {
                this._propertyNameToHandlerMap.Remove(propertyName);
                PropertyChangedEventManager.RemoveListener(propertySource, this, propertyName);
            }
            return (PropertyObserver<TPropertySource>)this;
        }
    }



}
