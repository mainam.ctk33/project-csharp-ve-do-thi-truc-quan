﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DoThiTrucQuan.Ultis;

namespace DoThiTrucQuan
{
    public partial class frmRename : Form
    {
        NodeView nodeView;
        public frmRename(NodeView n)
        {
            InitializeComponent();
            this.nodeView = n;
            this.Text = "Đổi tên đỉnh: " + n.Title.Text;
            cbDoiTen.DataSource = Flags.dsDinh;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            if (cbDoiTen.Text == "")
                MessageBox.Show("Chọn tên đỉnh muốn đổi thành");
            else
            {
                Flags.dsDinh.Remove(cbDoiTen.Text[0]);
                Flags.dsDinh.Add(nodeView.Title.Text[0]);
                nodeView.Title.Text = (nodeView.getNode.Title = cbDoiTen.Text[0]).ToString();
                this.Close();
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
