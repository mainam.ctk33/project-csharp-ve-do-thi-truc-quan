﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan.Layout
{
    public class CircularLayout:ILayout
    {

        Point Center;

        public void Execute(Graph g)
        {
            Center = new Point(1000/2, 500/2);
            if (g != null && g.Count > 0)
            {
                double angle = 0, radius = Center.Y - 25;
                double a = 2 * Math.PI / g.Count;
                foreach (Node n in g.listNode)
                {
                    double radian = angle;
                    n.LocationX = Math.Round(Math.Sin(radian) * radius + Center.X);
                    n.LocationY = Math.Round(Math.Cos(radian) * radius + Center.Y);
                    angle += a;
                }
            }
        }
    }
}
