﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan.Layout.Tree
{
    public class TreeLayout : ILayout
    {
        double width, height;
        Node node;
        Graph _graph;
        public TreeLayout(double w, double h)
        {
            width = w; height = h;
        }
        public int Depth()
        {
            daxet = new List<Node>();
            return Depth(node, 0, 0);
        }
        List<Node> daxet;
        private int Depth(Node node, int level, int depth)
        {
            bool cocon = false;
            List<Node> hangxom = _graph.HangXom(node);
            foreach (Node n in hangxom)
            {
                int sub_tree_depth=0;
                if (!daxet.Contains(n))
                {
                    daxet.Add(n);
                    sub_tree_depth = Depth(n, level + 1, depth);
                    cocon=true;
                }
                if (depth < sub_tree_depth)
                    depth = sub_tree_depth;

            }
            if (!cocon) return level;
            return depth;
        }
        public Queue<Node> GetAllNodesAtLevel(int level)
        {
            Queue<Node> nodes = new Queue<Node>();

            if (level <= Depth())
            {
                GetAllNodesAtLevel(nodes, node, level, 0);
            }

            return nodes;
        }
        private void GetAllNodesAtLevel(Queue<Node> nodes, Node current_node, int levelTo, int levelFrom)
        {
            if (levelFrom > levelTo)
                return;
            if (levelTo == levelFrom)
            {
                nodes.Enqueue(current_node);
                return;
            }

            List<Node> hangxom = _graph.HangXom(current_node);
            foreach (Node child in hangxom)
            {
                GetAllNodesAtLevel(nodes, child, levelTo, levelFrom + 1);
            }
        }
        public void Execute(Graph _graph)
        {
            if (_graph.Count == 1) return;
            this._graph = _graph;
            node = _graph.listNode[0];
            int muc1 = Depth();
            int muc = muc1;
            Point[][] positions = new Point[muc + 1][];
            Node[][] nodes = new Node[muc + 1][];
            Point Start = new Point(20, height);
            int radius = 20;
            int spacing;
            while (muc >= 0)
            {
                nodes[muc] = GetAllNodesAtLevel(muc).ToArray(); //danh sach node tai 1 level
                positions[muc] = new Point[nodes[muc].Length]; //vi tri cho tung nut tai 1 level
                spacing = (int)width / (nodes[muc].Length + 1);//chiue dai layout chia cho so luong nut cùng lever
                int y = (int)(height - radius) / muc1 * muc;
                if (muc == 0)
                    y += radius;
                for (int i = 0; i < nodes[muc].Length; i++)
                {
                    int x = spacing * (i + 1);
                    Point p = new Point(x, y);
                    positions[muc][i] = p;
                    nodes[muc][i].Location = new Point(p.X - radius, p.Y - radius);
                }
                muc--;
            }
        }
    }
}
