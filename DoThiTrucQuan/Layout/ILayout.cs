﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan.Layout
{
    public interface ILayout
    {
        void Execute(Graph g);

    }
}
