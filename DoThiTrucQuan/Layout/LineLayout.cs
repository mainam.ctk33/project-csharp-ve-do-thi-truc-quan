﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan.Layout
{
    public class LineLayout : ILayout
    {
        int width = 1000;
        int height = 500;
        public void Execute(Graph g)
        {
            int x_start = 50, y_start = height / 2, distance = width / g.Count, distance2 = height / g.Count;
            List<Node> kq = g.listNode;
            kq[0].LocationX = x_start;
            kq[0].LocationY = y_start;
            int y_start1 = y_start;
            for (int i = 1; i < g.Count - 1; i++)
            {
                if (i % 2 == 1)
                {
                    y_start -= distance2;
                    if (y_start <= 0)
                    {
                        y_start += distance2;
                    }
                    kq[i].LocationY = y_start;
                }
                if (i % 2 == 0)
                {
                    y_start1 += distance2;
                    if (y_start1 >= height)
                        y_start1 -= distance2;
                    kq[i].LocationY = y_start1;
                }

                x_start = x_start + distance;
                kq[i].LocationX = x_start;
            }
            kq[g.Count - 1].LocationX = x_start + distance;
            kq[g.Count - 1].LocationY = height / 2;
        }

    }
}
