﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan.Layout
{
    public class TreeLayoutBFS : ILayout
    {
        double _YRoot = 100;
        int level = 0, countG = 1;
        List<Node> listNode = new List<Node>();
        Queue<Node> queue = new Queue<Node>();
        List<Graph> listGraph = new List<Graph>();
        public TreeLayoutBFS() { }

        public void Execute(Graph g)
        {
            FindListSubGraph(g);
            foreach (var item in listGraph)
            {
                Node root = FindNodeRoot(item);
                root.Level = level;
                listNode.Add(root);
                Calculate(g, root);
                countG++;
            }

            while (true)
            {
                int sumLevel = 0;
                foreach (var i in listNode)
                    if (i.Level == level)
                        sumLevel++;
                if (sumLevel == 0) return;
                else
                {
                    int count = 1;
                    foreach (var i in listNode)
                        if (i.Level == level && count <= sumLevel)
                        {
                            double a = 800 * count / (sumLevel + 1);
                            double b = _YRoot + 100 * i.Level;
                            i.Location = new Point(a, b);
                            count++;
                        }
                    level++;
                }
            }
        }

        private void Calculate(Graph g, Node node)
        {
            int sum = 0, count = 1;
            List<Node> hangxom = node._graph.HangXom(node);
            if (hangxom.Count > 0)
            {
                sum = hangxom.Count;
                foreach (Node n in hangxom)
                {
                    if (!listNode.Contains(n))
                    {
                        listNode.Add(n);
                        queue.Enqueue(n);
                        n.Level = node.Level + 1;
                    }
                    count++;
                }
                while (queue.Count > 0)
                {
                    Node tmp = queue.Dequeue();
                    Calculate(g, tmp);
                }
            }
        }

        private void FindListSubGraph(DoThiTrucQuan.Graphs.Graph g)
        {
            List<Node> nodesChecked = new List<Node>();
            foreach (var n in g.listNode)
            {
                if (!nodesChecked.Contains(n))
                {
                    nodesChecked.Add(n);
                    Graph graph = new Graph();
                    graph.addNode(n);
                    Queue<Node> q = new Queue<Node>();
                    q.Enqueue(n);
                    while (q.Count > 0)
                    {
                        Node node = q.Dequeue();
                        List<Node> hangxom = node._graph.HangXom(node);
                        if (hangxom.Count == 0) break;
                        else
                            foreach (var _node in hangxom)
                                if (!nodesChecked.Contains(_node))
                                {
                                    nodesChecked.Add(_node);
                                    q.Enqueue(_node);
                                    graph.addNode(_node);
                                }
                    }
                    listGraph.Add(graph);
                }
            }
        }

        private Node FindNodeRoot(Graph _graph)
        {
            int count = 0, i;
            foreach (Node item in _graph.listNode)
            {
                List<Node> hangxom = item._graph.HangXom(item);
                if (hangxom.Count > count)
                    count = hangxom.Count;
            }
            for (i = 0; i < _graph.Count; i++)
            {
                List<Node> hangxom = _graph.listNode[i]._graph.HangXom(_graph.listNode[i]);
                if (hangxom.Count == count)
                    return _graph.listNode[i];
            }
            return null;
        }

    }
}
