﻿using System.Windows;
using System.Windows.Documents;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan
{
    /// <summary>
    /// Hosts a NodeConnectionAdorner in the adorner layer.
    /// </summary>
    public class NodeConnectionAdornerDecorator
        // Derive from AdornerDecorator in case the 
        // Window that hosts the GraphView has a custom
        // template that does not include an AdornerLayer.
        : AdornerDecorator
    {
        #region Constructor
        public NodeConnectionAdorner nodeConnect;

        public NodeConnectionAdornerDecorator()
        {
            base.Loaded += this.OnLoaded;
            nodeConnect = new NodeConnectionAdorner(this);
        }

        #endregion // Constructor

        #region OnLoaded

        void OnLoaded(object sender, RoutedEventArgs e)
        {
            var layer = AdornerLayer.GetAdornerLayer(this);
            if (layer == null)
                return;

            nodeConnect = new NodeConnectionAdorner(this);
            layer.Add(nodeConnect);
            this.GiveGraphToAdorner();
        }

        #endregion // OnLoaded

        #region Graph (DP)

        public Graph Graph
        {
            get { return (Graph)GetValue(GraphProperty); }
            set { SetValue(GraphProperty, value); }
        }

        public static readonly DependencyProperty GraphProperty =
            DependencyProperty.Register(
            "Graph",
            typeof(Graph),
            typeof(NodeConnectionAdornerDecorator),
            new UIPropertyMetadata(null, OnGraphChanged));

        static void OnGraphChanged(DependencyObject sender, DependencyPropertyChangedEventArgs e)
        {
            (sender as NodeConnectionAdornerDecorator).GiveGraphToAdorner();
        }

        #endregion // Graph (DP)

        #region Private Helpers

        void GiveGraphToAdorner()
        {
            if (nodeConnect != null && this.Graph != null)
            {
                nodeConnect.Graph = this.Graph;
            }
        }

        #endregion // Private Helpers

        #region Fields

        //   public NodeConnectionAdorner _adorner;


        #endregion // Fields
    }
}