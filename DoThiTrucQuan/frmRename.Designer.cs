﻿namespace DoThiTrucQuan
{
    partial class frmRename
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnOK = new System.Windows.Forms.Button();
            this.btnCancel = new System.Windows.Forms.Button();
            this.cbDoiTen = new System.Windows.Forms.ComboBox();
            this.lbTenDinh = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btnOK
            // 
            this.btnOK.Location = new System.Drawing.Point(12, 40);
            this.btnOK.Name = "btnOK";
            this.btnOK.Size = new System.Drawing.Size(75, 23);
            this.btnOK.TabIndex = 0;
            this.btnOK.Text = "Đổi tên";
            this.btnOK.UseVisualStyleBackColor = true;
            this.btnOK.Click += new System.EventHandler(this.btnOK_Click);
            // 
            // btnCancel
            // 
            this.btnCancel.Location = new System.Drawing.Point(93, 40);
            this.btnCancel.Name = "btnCancel";
            this.btnCancel.Size = new System.Drawing.Size(75, 23);
            this.btnCancel.TabIndex = 0;
            this.btnCancel.Text = "Hủy";
            this.btnCancel.UseVisualStyleBackColor = true;
            this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
            // 
            // cbDoiTen
            // 
            this.cbDoiTen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbDoiTen.FormattingEnabled = true;
            this.cbDoiTen.Items.AddRange(new object[] {
            "C"});
            this.cbDoiTen.Location = new System.Drawing.Point(93, 13);
            this.cbDoiTen.Name = "cbDoiTen";
            this.cbDoiTen.Size = new System.Drawing.Size(75, 21);
            this.cbDoiTen.TabIndex = 1;
            // 
            // lbTenDinh
            // 
            this.lbTenDinh.AutoSize = true;
            this.lbTenDinh.Location = new System.Drawing.Point(12, 20);
            this.lbTenDinh.Name = "lbTenDinh";
            this.lbTenDinh.Size = new System.Drawing.Size(51, 13);
            this.lbTenDinh.TabIndex = 2;
            this.lbTenDinh.Text = "Tên mới: ";
            // 
            // frmRename
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(181, 71);
            this.ControlBox = false;
            this.Controls.Add(this.lbTenDinh);
            this.Controls.Add(this.cbDoiTen);
            this.Controls.Add(this.btnCancel);
            this.Controls.Add(this.btnOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "frmRename";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Đổi tên đỉnh";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnOK;
        private System.Windows.Forms.Button btnCancel;
        private System.Windows.Forms.ComboBox cbDoiTen;
        private System.Windows.Forms.Label lbTenDinh;

    }
}