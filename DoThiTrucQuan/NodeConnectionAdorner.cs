﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;
using System.Linq;
using DoThiTrucQuan.Graphs;
namespace DoThiTrucQuan
{
    /// <summary>
    /// A UI adorner that renders connectors between nodes in a graph.
    /// </summary>
    public class NodeConnectionAdorner : Adorner
    {
        #region Constructor

        public NodeConnectionAdorner(UIElement adornedElement)
            : base(adornedElement)
        {
            _graph = new Graph();
        }

        #endregion // Constructor

        #region Properties

        public Graph Graph
        {
            get { return _graph; }
            set
            {
                if (value == _graph)
                    return;

                _graph = value;

                if (_graph != null)
                    this.ProcessGraph();
            }
        }

        #endregion // Properties

        #region Private Helpers

        public void ProcessGraph()
        {
            this.RemoveConnectors();
            foreach (Line l in _graph.listLine)
                this.AddConnector(l);
            base.InvalidateMeasure();
        }
        public void RemoveConnectors()
        {
            try
            {
                foreach (Line connector in _graph.listLine)
                {
                    base.RemoveVisualChild(connector);
                    base.RemoveLogicalChild(connector);
                }
            }
            catch (Exception)
            {

            }
        }
        public void RemoveConnectors(Line l)
        {
            try
            {
                base.RemoveVisualChild(l);
                base.RemoveLogicalChild(l);
                base.InvalidateMeasure();
            }
            catch (Exception)
            {

            }
        }
        public void ReProcessGraph(ListLine dsLine)
        {
            this.RemoveConnectors();
            foreach (var line in dsLine)
                this.AddConnector(line);
            _graph.listLine = dsLine.Clone();
            base.InvalidateMeasure();
        }
        void AddConnector(Node startNode, Node endNode)
        {
            var connector = new Line(startNode, endNode);
            AddConnector(connector);
        }
        public void AddConnector(Line l)
        {
            try
            {

            base.AddVisualChild(l);
            base.AddLogicalChild(l);
            }
            catch (Exception)
            {
            }
        }

        #endregion // Private Helpers

        #region Base Class Overrides

        protected override IEnumerator LogicalChildren
        {
            get { return _graph.listLine.GetEnumerator(); }
        }

        protected override int VisualChildrenCount
        {
            get { return _graph.listLine.Count; }
        }

        protected override Visual GetVisualChild(int index)
        {
            return _graph.listLine[index];
        }

        protected override Size MeasureOverride(Size constraint)
        {
            foreach (var nodeConnector in _graph.listLine)
                nodeConnector.Measure(constraint);

            if (Double.IsInfinity(constraint.Width) || Double.IsInfinity(constraint.Height))
                return new Size(10000, 10000);

            return constraint;
        }

        protected override Size ArrangeOverride(Size finalSize)
        {
            var bounds = VisualTreeHelper.GetDescendantBounds(base.AdornedElement);

            if (!bounds.IsEmpty)
                foreach (var nodeConnector in _graph.listLine)
                    nodeConnector.Arrange(bounds);

            return finalSize;
        }

        #endregion // Base Class Overrides

        #region Fields

        public Graph _graph;

        #endregion // Fields

    }
}