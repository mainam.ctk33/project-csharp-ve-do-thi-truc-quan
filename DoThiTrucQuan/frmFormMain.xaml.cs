﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using DoThiTrucQuan.Layout;
using System.IO;
using DoThiTrucQuan.Ultis;
using DoThiTrucQuan.Algorithm;
using DoThiTrucQuan.Layout.Tree;
using DoThiTrucQuan.Graphs;
using System.Globalization;
using System.Threading;

namespace DoThiTrucQuan
{
    /// <summary>
    /// Interaction logic for frmFormMain.xaml
    /// </summary>
    public partial class frmFormMain : Window
    {
        Button btn;
        ListLine _listLine;
        ILayout ilayout;
        public frmFormMain()
        {
            InitializeComponent();
            btn = btnFreeLayout;
            btn.IsEnabled = false;
            this.Loaded += new RoutedEventHandler(AppWindow_Loaded);
            EventHandleClass.xoaNode += new EventHandler(EventHandleClass_removeNode);
            EventHandleClass.ketNoi += new EventHandler(EventHandleClass_ketNoi);
            EventHandleClass.xoaKetNoi += new EventHandler(EventHandleClass_xoaKetNode);
        }
        public frmFormMain(Graph g)
        {
            InitializeComponent();
            graphView.DataContext = g;
        }

        void EventHandleClass_xoaKetNode(object sender, EventArgs e)
        {
            Node a = (sender as Node);
            Node b = Flags.NodeSelect.getNode;
            if (a != null && b != null)
            {
                Line l = _graph.listLine.removeLine(a, b);
                graphView.nodeConnect.nodeConnect.RemoveConnectors(l);
                ReLoadGraph();
                _maTrix.IsExpanded = false;
                ReLoadMaTrix();
            }

        }

        void EventHandleClass_ketNoi(object sender, EventArgs e)
        {
            Node a = (sender as Node);
            Node b = Flags.NodeSelect.getNode;
            if (a != null && b != null)
            {
                _graph.listLine.Add(new Line(a, b));
                graphView.nodeConnect.nodeConnect.ProcessGraph();
                ReLoadGraph();
                _maTrix.IsExpanded = false;
                ReLoadMaTrix();
            }
        }

        void EventHandleClass_removeNode(object sender, EventArgs e)
        {
            Node n = sender as Node;
            _graph.RemoveNode(n);
            ReLoadGraph();
            foreach (var l in _graph.listLine.findByNode(n))
            {
                _graph.listLine.Remove(l);
                graphView.nodeConnect.nodeConnect.RemoveConnectors(l);
            }
            Flags.dsDinh.Remove(n.Title);
            ReLoadDsDinh();
            _maTrix.IsExpanded = false;
            ReLoadMaTrix();
        }
        Graph _graph;

        void AppWindow_Loaded(object sender, RoutedEventArgs e)
        {
            Flags.dsDinh = new List<char>();
            for (char a = 'A'; a <= 'Z'; a++)
            {
                Flags.dsDinh.Add(a);
            }
            _graph = new Graph();
        }



        private void graphView_MouseDown(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (btn == btnFreeLayout && e.LeftButton == MouseButtonState.Pressed)
            {
                if (Flags.dsDinh.Count > 0)
                {
                    GraphView gv = sender as GraphView;
                    Node n = new Node(Flags.dsDinh[0], e.GetPosition(gv).X - 40, e.GetPosition(gv).Y - 40, _graph);
                    Flags.dsDinh.RemoveAt(0);
                    _graph.listNode.Add(n);
                    ReLoadGraph();
                    ReLoadDsDinh();
                    _maTrix.IsExpanded = false;
                    ReLoadMaTrix();
                    Flags.NodeSelect = null;
                }
                else
                    MessageBox.Show("Bạn đã nhập quá nhiều đỉnh");
            }

        }

        void ReLoadGraph()
        {
            graphView.DataContext = null;
            graphView.DataContext = _graph;
        }
        void ReLoadDsDinh()
        {
            List<Char> dsDinh = _graph.getList();
            cbDinhDau.ItemsSource = dsDinh;
            cbDinhDau.SelectedIndex = 0;
            cbDinhCuoi.ItemsSource = dsDinh;
            cbDinhDau.SelectedIndex = 0;
        }
        void ReLoadMaTrix()
        {
            try
            {
                if (_maTrix.IsExpanded)
                {
                    _matrix.DataContext = null;
                    _matrix.DataContext = _graph;
                }
            }
            catch (Exception)
            {
                _maTrix.IsExpanded = false;
            }
        }

        private void btnFreeLayout_Click(object sender, RoutedEventArgs e)
        {
            Flags.IsFreeLayout = true;
            btn.IsEnabled = true;
            btn = btnFreeLayout;
            btn.IsEnabled = false;
            expander.IsExpanded = true;
        }

        private void btnCircularLayout_Click(object sender, RoutedEventArgs e)
        {
            if (_graph.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("Không có đỉnh nào trong đồ thị");
                return;
            }
            Flags.IsFreeLayout = false;
            btn.IsEnabled = true;
            btn = btnCircularLayout;
            ilayout = new CircularLayout();
            ilayout.Execute(_graph);
            ReLoadGraph();
            btn.IsEnabled = false;
            expander.IsExpanded = false;
        }

        private void btnTreeLayoutBFS_Click(object sender, RoutedEventArgs e)
        {
            if (_graph.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("Không có đỉnh nào trong đồ thị");
                return;
            }
            Flags.IsFreeLayout = false;
            btn.IsEnabled = true;
            btn = btnTreeLayoutBFS;
            ilayout = new TreeLayoutBFS();
            ilayout.Execute(_graph);
            graphView.nodeConnect.nodeConnect.ProcessGraph();
            ReLoadGraph();
            btn.IsEnabled = false;
            expander.IsExpanded = false;
        }

        private void btnTreeLayout_Click(object sender, RoutedEventArgs e)
        {
            if (_graph.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("Không có đỉnh nào trong đồ thị");
                return;
            }
            if (CayBaoTrum.SoThanhPhanLienThong(_graph).Count > 1)
            {
                System.Windows.Forms.MessageBox.Show("Đồ thị không liên thông, không thể tìm được cây bao trùm");
                return;
            }
            Flags.IsFreeLayout = false;
            expander.IsExpanded = false;
            btn.IsEnabled = true;
            btn = btnTreeLayout;
            ilayout = new TreeLayout(700, 700);
            ilayout.Execute(_graph);
            ReLoadGraph();
            btn.IsEnabled = false;
            expander.IsExpanded = false;

        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.OpenFileDialog op = new System.Windows.Forms.OpenFileDialog();
            op.Title = "Chọn file";
            op.Filter = "(Xml File)|*xml";
            if (op.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                LoadData(op.FileName);
            }
        }
        void LoadData(string FileName)
        {
            Flags.dsDinh = new List<char>();
            for (char a = 'A'; a <= 'Z'; a++)
            {
                Flags.dsDinh.Add(a);
            }
            _listLine = new ListLine();
            _graph = GraphBuilder.BuildGraphs(Flags.dsDinh, FileName, _listLine);
            graphView.nodeConnect.nodeConnect.ReProcessGraph(new ListLine());
            _graph.listLine = _listLine.Clone();
            ReLoadDsDinh();
            ReLoadGraph();
        }

        private void MenuItem_Click_1(object sender, RoutedEventArgs e)
        {
            System.Windows.Forms.SaveFileDialog sf = new System.Windows.Forms.SaveFileDialog();
            sf.Title = "Save as";
            sf.Filter = "(Xml File)|*.xml";
            if (sf.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                StreamWriter st = new StreamWriter(sf.FileName);
                st.WriteLine("<?xml version=\"1.0\" encoding=\"utf-8\" ?>");
                st.WriteLine("<graph>");
                foreach (Node n in _graph.listNode)
                {
                    st.WriteLine(string.Format("<node id=\"{0}\" x=\"{1}\" y=\"{2}\">", n.Title, n.LocationX, n.LocationY));
                    foreach (Node node in _graph.HangXom(n))
                        st.WriteLine(String.Format("<dependency id=\"{0}\" />", node.Title));
                    st.WriteLine("</node>");
                }
                st.WriteLine("</graph>");
                st.Close();
            }
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e)
        {
            if (expander.IsExpanded == true)
            {
                if (btn != null)
                    btn.IsEnabled = true;
                btn = btnFreeLayout;
                btnFreeLayout_Click(null, null);
            }
        }

        private void TimDuongDi_Click(object sender, RoutedEventArgs e)
        {
            foreach (var n in _graph.listNode)
                n.ChangeColor(Colors.LightGreen);
            Flags.NodeSelect = null;
            txtChuTrinh.Text = "";
            if (cbDinhCuoi.Text == cbDinhDau.Text)
                txtChuTrinh.Text = String.Format("{0} -> {0}", cbDinhDau.Text, cbDinhCuoi.Text);
            else
            {
                Node nodea = _graph.getNode(cbDinhDau.Text[0]);
                Node nodeb = _graph.getNode(cbDinhCuoi.Text[0]);
                Dictionary<Node, Node> kq = TimDuongDi.Execute(_graph, nodea, nodeb);
                if (kq.Count == 0)
                {
                    txtChuTrinh.Text = "Không có đường đi giữa hai đỉnh đồ thị";
                    return;
                }
                Node parent;
                nodeb.ChangeColor(Colors.Red);
                while ((parent = kq[nodeb]) != null)
                {
                    parent.ChangeColor(Colors.Red);
                    txtChuTrinh.Text = String.Format("{0} -> {1}\n", parent.Title, nodeb.Title) + txtChuTrinh.Text;
                    nodeb = parent;
                    if (parent == nodea) return;
                }
            }
        }

        private void MenuItem_Click_2(object sender, RoutedEventArgs e)
        {
            int kq = Algorithm.CayBaoTrum.SoThanhPhanLienThong(_graph).Count;
            System.Windows.Forms.MessageBox.Show("Đồ thị có " + kq + " thành phần liên thông");
        }

        private void btnSpanningTree_Click(object sender, RoutedEventArgs e)
        {
            if (_graph.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("Không có đỉnh nào trong đồ thị");
                return;
            }
            if (CayBaoTrum.SoThanhPhanLienThong(_graph).Count > 1)
            {
                System.Windows.Forms.MessageBox.Show("Đồ thị không liên thông, không thể tìm được cây bao trùm");
                return;
            }
            Flags.IsFreeLayout = false;
            _listLine = _graph.listLine.Clone();
            btn.IsEnabled = true;
            btn = btnSpanningTree;
            ListLine ls = Kruskal.Execute(_graph.listLine);
            graphView.nodeConnect.nodeConnect.ReProcessGraph(ls);
            _graph.ilayout = new TreeLayout(graphView.Width, 500);
            _graph.Draw();
            ReLoadGraph();
            btn.IsEnabled = false;
            expander.IsExpanded = false;
        }

        private void btnLineLayout_Click(object sender, RoutedEventArgs e)
        {
            if (_graph.Count == 0)
            {
                System.Windows.Forms.MessageBox.Show("Không có đỉnh nào trong đồ thị");
                return;
            }
            Flags.IsFreeLayout = false;
            btn.IsEnabled = true;
            btn = btnLineLayout;
            _graph.ilayout = new LineLayout();
            _graph.Draw();
            ReLoadGraph();
            btn.IsEnabled = false;
            expander.IsExpanded = false;
        }
        private void _maTrix_Expanded(object sender, RoutedEventArgs e)
        {
            try
            {
                if (_graph.Count == 0 && _maTrix.IsExpanded)
                {
                    _maTrix.IsExpanded = false;
                    return;
                }
                ReLoadMaTrix();
            }
            catch (Exception)
            {

            }
        }

        private void btnSpanningTree_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            if (btnSpanningTree.IsEnabled == true)
            {
                _graph.listLine = _listLine.Clone();
                graphView.nodeConnect.nodeConnect.ProcessGraph();
                ReLoadGraph();
            }

        }

        public System.Drawing.Bitmap GetBitmap()
        {
            try
            {
                Thread.Sleep(1000);
                System.Drawing.Point StartPoint = new System.Drawing.Point(0, 0);
                System.Drawing.Bitmap bitmap = null;
                this.Dispatcher.Invoke(new System.Windows.Forms.MethodInvoker(() =>
            {
                bitmap = new System.Drawing.Bitmap(Convert.ToInt32(graphView.Width), Convert.ToInt32(graphView.Height));
                System.Drawing.Graphics g = System.Drawing.Graphics.FromImage(bitmap);
                Thread.Sleep(500);
                g.CopyFromScreen(StartPoint, System.Drawing.Point.Empty, new System.Drawing.Size(Convert.ToInt32(System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width), Convert.ToInt32(this.Height - 10)));
                unfullScreen();
            }));
                return bitmap;
            }
            catch (Exception)
            {
                return null;
            }
        }

        void fullScreen()
        {
            this.ResizeMode = System.Windows.ResizeMode.NoResize;
            this.WindowStyle = System.Windows.WindowStyle.None;
            this.Width = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Width;
            this.Height = System.Windows.Forms.Screen.PrimaryScreen.WorkingArea.Height;
            this.menu1.Height = this.exRose.Height = this.exMatrix.Width = this.exTimDuong.Width = 0;
        }
        void unfullScreen()
        {
            this.ResizeMode = System.Windows.ResizeMode.NoResize;
            this.WindowStyle = System.Windows.WindowStyle.SingleBorderWindow;
            this.WindowState = System.Windows.WindowState.Maximized;
            this.menu1.Height = 23;
            this.exRose.Height = 105;
            this.exMatrix.Width = this.exTimDuong.Width = Double.NaN;
        }
        System.Windows.Forms.SaveFileDialog sv;
        void Save()
        {
            Thread.Sleep(500);
            GetBitmap().Save(sv.FileName);
        }
        private void MenuItem_Click_3(object sender, RoutedEventArgs e)
        {
            sv = new System.Windows.Forms.SaveFileDialog();
            sv.Filter = "PNG File|*.png|JPG File|*.jpg";
            if (sv.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                fullScreen();
                Thread t = new Thread(Save);
                t.Start();
            }
        }
    }
}
