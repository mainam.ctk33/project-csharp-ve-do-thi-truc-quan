﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using DoThiTrucQuan.Ultis;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan
{
    /// <summary>
    /// Interaction logic for NodeView.xaml
    /// </summary>
    public partial class NodeView : UserControl
    {
        MenuItem mn2;
        ContextMenu ctmn;
        public NodeView()
        {
            InitializeComponent();
            this.MouseRightButtonUp += new MouseButtonEventHandler(NodeView_MouseRightButtonUp);
            ctmn = new ContextMenu();
            MenuItem mn = new MenuItem() { Header = "Xóa" };
            mn.Click += new RoutedEventHandler(mn_Click);
            ctmn.Items.Add(mn);
            mn = new MenuItem() { Header = "Chọn" };
            mn.Click += new RoutedEventHandler(mn1_Click);
            ctmn.Items.Add(mn);
            mn2 = new MenuItem() { Header = "Tạo liên kết với đỉnh được chọn" };
            mn2.Click += new RoutedEventHandler(mn2_Click);
            ctmn.Items.Add(mn2);
            MenuItem mn3 = new MenuItem() { Header = "Đổi tên" };
            mn3.Click += new RoutedEventHandler(mn3_Click);
            ctmn.Items.Add(mn3);
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(NodeView_DataContextChanged);
        }

        void NodeView_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            this.getNode.changeColor += new EventHandler(getNode_changeColor);
        }

        void getNode_changeColor(object sender, EventArgs e)
        {
            this.border.Background = new SolidColorBrush((Color)sender);
        }

        void mn3_Click(object sender, RoutedEventArgs e)
        {
            frmRename rn = new frmRename(this);
            rn.ShowDialog();
        }

        void NodeView_MouseRightButtonUp(object sender, MouseButtonEventArgs e)
        {
            mn2.IsEnabled = (Ultis.Flags.NodeSelect != null && Flags.NodeSelect != this);
            if (Flags.NodeSelect != null)
                mn2.Header = (getNode._graph.HangXom(getNode).Contains(Flags.NodeSelect.getNode)) ? "Hủy liên kết với đỉnh được chọn" : "Tạo liên kết với đỉnh được chọn";
            if (Flags.IsFreeLayout)
                this.ContextMenu = ctmn;
            else this.ContextMenu = null;
        }

        public Node getNode
        {
            get
            {
                if (this.DataContext == null) return null;
                return this.DataContext as Node;
            }
        }

        void mn2_Click(object sender, RoutedEventArgs e)
        {
            if (Flags.NodeSelect != null)
            {
                Flags.NodeSelect.border.Background = new SolidColorBrush(Colors.LightGreen);
                if (getNode._graph.HangXom(getNode).Contains(Flags.NodeSelect.getNode))
                    EventHandleClass.XoaKetNoi(this.getNode);
                else
                    EventHandleClass.KetNoi(this.getNode);
            }
            Flags.NodeSelect = null;
        }
        void mn1_Click(object sender, RoutedEventArgs e)
        {
            if (Flags.NodeSelect != null) Flags.NodeSelect.border.Background = new SolidColorBrush(Colors.LightGreen);
            Flags.NodeSelect = this;
            Flags.NodeSelect.border.Background = new SolidColorBrush(Colors.Red);
        }

        void mn_Click(object sender, RoutedEventArgs e)
        {
            Node n = this.DataContext as Node;
            Flags.NodeSelect = null;
            EventHandleClass.RemoveNode(n);
        }
    }
}
