﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;

namespace DoThiTrucQuan.Graphs
{
    public partial class Matrix : UserControl
    {
        public Matrix()
        {
            InitializeComponent();
            this.DataContextChanged += new DependencyPropertyChangedEventHandler(Matrix_DataContextChanged);
        }

        Button CreateButton(object Content, Color color)
        {
            Button btn = new Button();
            btn.Background = new SolidColorBrush(color);
            btn.Width = 30;
            btn.Height = 30;
            btn.Content = Content;
            return btn;
        }

        void Matrix_DataContextChanged(object sender, DependencyPropertyChangedEventArgs e)
        {
            Graph _graph = this.DataContext as Graph;
            _matrix.ColumnDefinitions.Clear();
            _matrix.RowDefinitions.Clear();
            if (_graph != null)
            {
                Button rect = CreateButton("", Colors.Gray);
                Grid.SetRow(rect, 0);
                Grid.SetColumn(rect, 0);
                _matrix.Children.Add(rect);

                for (int i = 0; i < _graph.Count + 1; i++)
                {
                    _matrix.ColumnDefinitions.Add(new ColumnDefinition() { Width = new GridLength(30) });
                    _matrix.RowDefinitions.Add(new RowDefinition() { Height = new GridLength(30) });
                    if (i > 0)
                    {
                        rect = CreateButton(_graph.listNode[i - 1], Colors.Gray);
                        Grid.SetRow(rect, i);
                        Grid.SetColumn(rect, 0);
                        _matrix.Children.Add(rect);
                        rect = CreateButton(_graph.listNode[i - 1], Colors.Gray);
                        Grid.SetRow(rect, 0);
                        Grid.SetColumn(rect, i);
                        rect.Content = _graph.listNode[i - 1];
                        _matrix.Children.Add(rect);
                    }
                }
                for (int i = 1; i < _graph.Count + 1; i++)
                    for (int j = 1; j <= i; j++)
                    {
                        List<Line> l = _graph.listLine.findByNode(_graph.listNode[i - 1], _graph.listNode[j - 1]);
                        rect = CreateButton((l.Count != 0 ? 1 : 0), (l.Count == 0 ? Colors.White : Colors.Yellow));
                        Grid.SetRow(rect, i);
                        Grid.SetColumn(rect, j);
                        _matrix.Children.Add(rect);

                        rect = CreateButton((l.Count != 0 ? 1 : 0), (l.Count == 0 ? Colors.White : Colors.Yellow));
                        Grid.SetRow(rect, j);
                        Grid.SetColumn(rect, i);
                        _matrix.Children.Add(rect);
                    }

            }
        }
    }
}
