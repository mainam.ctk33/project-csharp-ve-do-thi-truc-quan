﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml.Linq;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan
{
    public class GraphBuilder
    {
        public static Graph BuildGraphs(List<char> dsDinh, string filename, ListLine listLine)
        {
            return BuildGraph(filename,dsDinh, listLine);
        }

        static Graph BuildGraph(string xmlFileName,List<char> dsDinh, ListLine listLine)
        {
            XDocument xdoc = XDocument.Load(xmlFileName);

            // Create a graph.
            var graphElem = xdoc.Element("graph");
            var graph = new Graph();

            var nodeElems = graphElem.Elements("node").ToList();

            // Create all of the nodes and add them to the graph.
            foreach (XElement nodeElem in nodeElems)
            {
                char name = nodeElem.Attribute("id").Value[0];
                double localx = (double)nodeElem.Attribute("x");
                double localy = (double)nodeElem.Attribute("y");

                var node = new Node(name, localx, localy,graph);
                graph.addNode(node);
                dsDinh.Remove(name);
            }

            // Associate each node with its dependencies.
            foreach (Node node in graph.listNode)
            {
                var nodeElem = nodeElems.First(elem => elem.Attribute("id").Value[0] == node.Title);
                var dependencyElems = nodeElem.Elements();
                foreach (XElement dependencyElem in dependencyElems)
                {
                    char nodeName = dependencyElem.Attribute("id").Value[0];
                    if (nodeName == node.Title)
                        throw new Exception("A node cannot be its own dependency.  Node ID = " + nodeName);
                    var dependency = graph.listNode.FirstOrDefault(n => n.Title == nodeName);
                    if (dependency != null)
                        listLine.add(node, dependency);
                }
            }
            return graph;
        }
    }
}