﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan.Algorithm
{
    class TimDuongDi
    {
        static List<Node> daxet;
        public static Dictionary<Node, Node> Execute(Graph _graph, Node a, Node b)
        {
            Dictionary<Node, Node> kq = new Dictionary<Node, Node>();
            daxet = new List<Node>();
            Queue<Node> q = new Queue<Node>();
            if (_graph.listLine.findByNode(a, b).Count > 0)
            {
                kq.Add(b, a);
                return kq;
            }

            foreach (Node n in _graph.HangXom(a))
                if (!daxet.Contains(n))
                {
                    daxet.Add(n);
                    kq.Add(n, a);
                    q.Enqueue(n);
                }

            while (q.Count > 0)
            {
                Node node = q.Dequeue();
                foreach (Node n in _graph.HangXom(node))
                    if (!daxet.Contains(n))
                    {
                        kq.Add(n, node);
                        if (n == b)
                            return kq;
                        daxet.Add(n);
                        q.Enqueue(n);
                    }
            }
            return new Dictionary<Node, Node>();
        }
    }
}
