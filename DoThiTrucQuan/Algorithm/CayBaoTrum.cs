﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan.Algorithm
{
    public class CayBaoTrum
    {
        public static List<Graph> SoThanhPhanLienThong(Graph _graph)
        {
            List<Graph> kq = new List<Graph>();
            List<Node> daxet = new List<Node>();
            Queue<Node> q = new Queue<Node>();
            foreach (var n in _graph.listNode)
            {
                if (!daxet.Contains(n))
                {
                    Graph tmp = new Graph();
                    daxet.Add(n);
                    q.Enqueue(n);
                    tmp.addNode(n);
                    while (q.Count > 0)
                    {
                        var node = q.Dequeue();
                        foreach (var _node in _graph.HangXom(node))
                            if (!daxet.Contains(_node))
                            {
                                q.Enqueue(_node);
                                daxet.Add(_node);
                            }
                    }
                    kq.Add(tmp);
                }

            }
            return kq;
        }
    }
}
