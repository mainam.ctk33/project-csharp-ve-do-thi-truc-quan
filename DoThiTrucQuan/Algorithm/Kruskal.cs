﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan
{
    public class Kruskal
    {
        public static ListLine Execute(ListLine listLine)
        {
            ListLine tmp = new ListLine();
            ListLine spanningTree = new ListLine();
            List<Node> dsDinh2 = new List<Node>();
            Queue<Node> q = new Queue<Node>();
            while (tmp.Distinct().Count<Line>() != listLine.Count)
            {
                foreach (var l in listLine)
                {
                    if (tmp.Contains(l))
                        continue;
                    q.Enqueue(l._startNode);
                    q.Enqueue(l._endNode);
                    dsDinh2.Add(l._startNode);
                    dsDinh2.Add(l._endNode);
                    spanningTree.Add(l);
                    while (q.Count > 0)
                    {
                        Node n = q.Dequeue();
                        List<Line> dsl = listLine.findByNode(n);
                        foreach (var ln in dsl)
                        {
                            if (ln._endNode == n && !dsDinh2.Contains(ln._startNode))
                            {
                                q.Enqueue(ln._startNode);
                                spanningTree.Add(ln);
                                dsDinh2.Add(ln._startNode);
                            }
                            if (ln._startNode == n && !dsDinh2.Contains(ln._endNode))
                            {
                                q.Enqueue(ln._endNode);
                                spanningTree.Add(ln);
                                dsDinh2.Add(ln._endNode);
                            }
                            tmp.Add(ln);
                        }
                    }
                }
            }
            return spanningTree;
        }
    }
}
