﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Windows.Markup;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan
{
    /// <summary>
    /// Interaction logic for frmSaveToImage.xaml
    /// </summary>
    public partial class frmSaveToImage : Window
    {
        public frmSaveToImage()
        {
            InitializeComponent();
        }
        GraphView g;
        //public frmSaveToImage(GraphView g)
        //{
        //    InitializeComponent();
        //    grid.Children.Add(g);
        //    this.Closing += new System.ComponentModel.CancelEventHandler(frmSaveToImage_Closing);
        //    g.nodeConnect.DataContext = null;
        //    g.nodeConnect.DataContext = g.DataContext;
        //    //this.Topmost = true;
        //    //graphView.DataContext = null;
        //    //graphView.nodeConnect = g.nodeConnect;
        //    //NodeConnectionAdorner.getNodeConnectionAdorner.ProcessGraph();
        //}
        GraphView gv;
        NodeConnectionAdornerDecorator node;
        public frmSaveToImage(GraphView gr)
        {
            InitializeComponent();
            this.gv = gr;
            node = gv.nodeConnect;
            gv.dock.Children.Remove(node);
            this.grid.Children.Add(node);
            //node.nodeConnect._graph = gr.DataContext as Graph;
            //node.nodeConnect.ProcessGraph();
            this.Closing += new System.ComponentModel.CancelEventHandler(frmSaveToImage_Closing);
        }

        void frmSaveToImage_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            grid.Children.Remove(node);
            gv.dock.Children.Add(node);
            gv.nodeConnect.nodeConnect.ProcessGraph();
        }
    }
}
