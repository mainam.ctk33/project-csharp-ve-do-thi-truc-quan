﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoThiTrucQuan.Graphs;

namespace DoThiTrucQuan
{
    public class EventHandleClass
    {
        public static event EventHandler xoaNode;
        public static event EventHandler ketNoi;
        public static event EventHandler xoaKetNoi;
        public static void RemoveNode(Node n)
        {
            if (xoaNode != null) 
            xoaNode(n, null);
        }
        public static void KetNoi(Node n)
        {
            if (ketNoi != null)
                ketNoi(n, null);
        }
        public static void XoaKetNoi(Node n)
        {
            if (xoaKetNoi != null)
                xoaKetNoi(n, null);
        }
    }
}
