﻿using System.Collections.Generic;
using System.Linq;
using DoThiTrucQuan.Observable;
using System.Windows;
using DoThiTrucQuan.Graphs;
using System.Windows.Media;
using System;

namespace DoThiTrucQuan.Graphs
{
    public class Node : ObservableObject
    {
        public Graph _graph;
        public event EventHandler changeColor;
            
        public Node(char title, double locationX, double locationY, Graph _graph)
        {
            
            this.LocationX = locationX;
            this.LocationY = locationY;
            this.Title = title;
            this._graph = _graph;
        }
        public char Title { get; set; }

        public void ChangeColor(Color color)
        {
            if (changeColor != null)
                changeColor(color, null);
        }

        public double LocationX
        {
            get { return _locationX; }
            set
            {
                if (value == _locationX)
                    return;

                _locationX = value;
                base.RaisePropertyChanged("LocationX");
            }
        }

        public double LocationY
        {
            get { return _locationY; }
            set
            {
                if (value == _locationY)
                    return;

                _locationY = value;

                base.RaisePropertyChanged("LocationY");
            }
        }
        public int Level;

        public double NodeWidth
        {
            get { return 30; }
        }

        public double NodeHeight
        {
            get { return 30; }
        }

        double _locationX, _locationY;

        public override string ToString()
        {
            return Title.ToString();
        }
        public Point Location
        {
            set
            {
                this.LocationX = value.X;
                this.LocationY = value.Y;
            }
        }
        public bool Equal(Node n)
        {
            if(n==null) return false;
            return this.Title.Equals(n.Title);
        }

    }
}