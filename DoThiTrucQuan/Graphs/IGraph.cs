﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DoThiTrucQuan.Layout;

namespace DoThiTrucQuan.Graphs
{
    interface IGraph
    {
        List<Node> listNode { get; set; }
        ListLine listLine { get; set; }
        bool addNode(Node node);
        bool addLine(Line line);
        ILayout ilayout { get; set; }
        void Draw();
    }
}
