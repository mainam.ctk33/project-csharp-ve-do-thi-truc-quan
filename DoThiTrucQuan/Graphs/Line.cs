﻿using System;
using System.Linq;
using System.Windows;
using System.Windows.Media.Animation;
using DoThiTrucQuan;
using DoThiTrucQuan.Observable;
using DoThiTrucQuan.LineConnect;
using System.Collections.Generic;

namespace DoThiTrucQuan.Graphs
{
    public class Line : LineBase
    {
        #region Constructor

        public Line(Node startNode, Node endNode)
        {
            _startNode = startNode;
            _endNode = endNode;

            this.SetToolTip();
            this.UpdateLocations(false);
            _startObserver = new PropertyObserver<Node>(_startNode)
                .RegisterHandler(n => n.LocationX, n => this.UpdateLocations(true))
                .RegisterHandler(n => n.LocationY, n => this.UpdateLocations(true));

            _endObserver = new PropertyObserver<Node>(_endNode)
                .RegisterHandler(n => n.LocationX, n => this.UpdateLocations(true))
                .RegisterHandler(n => n.LocationY, n => this.UpdateLocations(true));
        }

        #endregion // Constructor

        #region Private Helpers

        static Point ComputeLocation(Node node1, Node node2)
        {
            Point loc = new Point
            {
                X = node1.LocationX + (node1.NodeWidth / 2),
                Y = node1.LocationY + (node1.NodeHeight / 2)
            };

            bool overlapY = Math.Abs(node1.LocationY - node2.LocationY) < node1.NodeHeight;
            if (!overlapY)
            {
                bool above = node1.LocationY < node2.LocationY;
                if (above)
                    loc.Y += node1.NodeHeight / 2;
                else
                    loc.Y -= node1.NodeHeight / 2;
            }

            bool overlapX = Math.Abs(node1.LocationX - node2.LocationX) < node1.NodeWidth;
            if (!overlapX)
            {
                bool left = node1.LocationX < node2.LocationX;
                if (left)
                    loc.X += node1.NodeWidth / 2;
                else
                    loc.X -= node1.NodeWidth / 2;
            }

            return loc;
        }

        void SetToolTip()
        {
            string toolTipText = String.Format("Cạnh nối giữa hai đỉnh {0} và {1} ", _startNode.Title, _endNode.Title);
            base.ToolTip = toolTipText;
        }

        public bool Equals(Line tmp)
        {
            return tmp!=null&&(this._endNode == tmp._startNode || this._endNode == tmp._endNode) && (this._startNode == tmp._startNode || this._startNode == tmp._endNode);
        }

        void UpdateLocations(bool animate)
        {
            var start = ComputeLocation(_startNode, _endNode);
            var end = ComputeLocation(_endNode, _startNode);

            if (animate)
            {
                base.BeginAnimation(LineBase.X1Property, CreateAnimation(base.X1, start.X));
                base.BeginAnimation(LineBase.Y1Property, CreateAnimation(base.Y1, start.Y));
                base.BeginAnimation(LineBase.X2Property, CreateAnimation(base.X2, end.X));
                base.BeginAnimation(LineBase.Y2Property, CreateAnimation(base.Y2, end.Y));
            }
            else
            {
                base.X1 = start.X;
                base.Y1 = start.Y;
                base.X2 = end.X;
                base.Y2 = end.Y;
            }
        }

        static AnimationTimeline CreateAnimation(double from, double to)
        {
            return new DoubleAnimation
            {
                Duration = new Duration(TimeSpan.FromSeconds(0.1)),
                From = from,
                To = to
            };
        }

        #endregion // Private Helpers

        #region Fields

        public Node _startNode, _endNode;
        readonly PropertyObserver<Node> _startObserver, _endObserver;

        #endregion // Fields

        public override string ToString()
        {
            return _startNode + " " + _endNode;
        }

        public bool Equal(Line other)
        {
            return ((this._startNode.Equal(other._startNode)||this._startNode.Equal(other._endNode))&&(this._endNode.Equal(other._startNode)||this._endNode.Equal(other._endNode)));
        }
        public Line Clone()
        {
            return base.MemberwiseClone() as Line;
        }
    }
}