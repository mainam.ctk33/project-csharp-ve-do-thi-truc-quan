﻿using System.Collections.Generic;
using System.Linq;
using System;
using DoThiTrucQuan.Layout;

namespace DoThiTrucQuan.Graphs
{
    /// <summary>
    /// Represents a set of nodes that can be dependent upon each other, 
    /// and will detect circular dependencies between its nodes.
    /// </summary>
    public class Graph : IGraph
    {
        public List<Node> listNode { get; set; }
        public ListLine listLine { get; set; }
        public ILayout ilayout { get; set; }

        public Graph()
        {
            listNode = new List<Node>();
            listLine = new ListLine();
        }

        public bool addNode(Node node)
        {
            if (listNode.Find(x => x.Equal(node)) != null)
                return false;
            listNode.Add(node);
            return true;
        }
        public bool addLine(Line line)
        {
            if (listLine.Find(x => x.Equal(line)) != null)
                return false;
            listLine.Add(line);
            return true;
        }

        public List<char> getList()
        {
            return listNode.Select(x => x.Title).ToList<char>();
        }
        public int Count
        {
            get
            {
                return listNode.Count;
            }
        }
        public bool RemoveNode(Node n)
        {
            if (n != null)
                return listNode.Remove(n);
            return false;
        }

        public bool RemoveNode(string nodeName)
        {
            return listNode.Remove(listNode.Find(x => x.Title.Equals(nodeName)));
        }
        public Node getNode(char Title)
        {
            return listNode.Find(x => x.Title.Equals(Title));
        }
        public List<Node> HangXom(Node a)
        {
            List<Node> kq = new List<Node>();
            foreach (Line l in listLine)
            {
                if (l._endNode == a) kq.Add(l._startNode);
                else if (l._startNode == a) kq.Add(l._endNode);
            }
            return kq;
        }
        public Graph Clone()
        {
            return new Graph() { listLine = this.listLine.Clone(), listNode = this.listNode };
        }


        public void Draw()
        {
            if (ilayout != null)
                ilayout.Execute(this);
        }
    }
}