﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DoThiTrucQuan.Graphs
{
    public class ListLine : List<Line>
    {
        public ListLine() { }
        public ListLine(List<Line> listLine)
        {
            foreach (var l in listLine)
            {
                this.Add(l);
            }
        }

        public void add(Node a, Node b)
        {
            Line l = new Line(a, b);
            if (findByNode(a, b).Count != 0) return;
            this.Add(l);
        }
        public Line removeLine(Node a, Node b)
        {
            Line l = this.Find(x => (x._endNode == a || x._endNode == b) && (x._startNode == a || x._startNode == b));
            this.Remove(l);
            return l;
        }
        public List<Line> findByNode(Node a)
        {
            return this.FindAll(x => x._endNode == a || x._startNode == a);
        }
        public List<Line> findByNode(Node a, Node b)
        {
            return this.FindAll(x => (x._endNode == a && x._startNode == b) || (x._endNode == b && x._startNode == a));
        }

        public ListLine Clone()
        {
            //return new ListLine(this.Select(x => x.Clone()).ToList());
            return this;
        }

    }
}
